import java.util.*;
import java.util.concurrent.ThreadLocalRandom;

public class Main {

    public static void main(String[] args) {


        List<Integer> list=new ArrayList<>();
        list.add(4);
        list.add(1);
        list.add(100000000);


        System.out.println(solution(list));
        System.out.println(substring("ab","ababab"));

        float[] tablicaFloatow=new float[4];
        tablicaFloatow[0]=2;
        tablicaFloatow[1]=7;
        tablicaFloatow[2]=5;
        tablicaFloatow[3]=10;
        int[] rozwiazanie= new int[2];
        rozwiazanie=solution2(tablicaFloatow,9);
        System.out.println(rozwiazanie[0]+","+rozwiazanie[1]);
        testZad5();


    }
    static int solution(List<Integer> a){
        Iterator itr= a.iterator();
        if(a.size()>1E5 || a.size()==0){
            throw new ZlyRozmiar();
        }
        int min=a.get(0),i;
        while(itr.hasNext()){
            i=(Integer) itr.next();
            if(i>1E6 || i<-1E6)throw new ZlyRozmiarArgumentu();
            if(min>i)min=i;
        }
        return min;
    }
    static int substring(String a, String b){
        if(a.contains(b))return 0;
        int i=1;
        String a1=a;
        while(a.length()<b.length())
        {
            a+=a1;
            i++;
        }
        if(a.contains(b))return i;
        else {
            i++;
            a+=a1;
            if(a.contains(b))return i;
        }
        return -1;
    }
    static int [] solution2(float [] arr, float target){
        float []kopia=Arrays.copyOf(arr,arr.length);
        Arrays.sort(kopia);
        int j=kopia.length-1,i=0;
        while(i!=j)
        {
            if(kopia[i]+kopia[j]==target){
                break;
            }
            if(kopia[i]+kopia[j]>target)j--;
            else i++;
        }
        if(i==j) throw new NieMaRozwiazania();
        int[] tab=new int[2];
        for(int x=0;x<arr.length;x++)
        {
            if(arr[x]==kopia[i])tab[0]=x;
            if(arr[x]==kopia[j])tab[1]=x;

        }


        return tab;

 //    for(int i=0;i<arr.length-1;i++){
 //        for (int j=i+1;j<arr.length;j++){
 //            if(arr[i]+arr[j]==target){
 //                int[] tab=new int[2];
 //                tab[0]=i;
 //                tab[1]=j;
 //                return tab;
 //            }
 //        }
 //    }

    }
    public static void testZad5(){
        Zadanie5 test = new Zadanie5();
        int[] optimistic=new int[100000];
        for(int i=0;i<optimistic.length;i++)
        {
            optimistic[i]=i;
        }
        int[] pesymistic=new int[100000];//pesymistyczny
        for(int i=pesymistic.length-1,j=0;i>0;i--,j++)
        {
            pesymistic[j]=i;
        }
        int[] realistic=new int[100000];//realistyczny
        int randomNum;
        for(int i=0;i<realistic.length;i++)
        {
            randomNum = ThreadLocalRandom.current().nextInt(-10000, 10000);
            optimistic[i]=randomNum;
        }

        long tStart = System.currentTimeMillis();
        // test.bubbleSort(optimistic);//optymistyczny
        //   test.insertionSort(optimistic);//optymistyczny
        // test.quickSort(optimistic,0,optimistic.length-1);//optymistyczny
        //   test.heapSort(optimistic);//optymistyczny
        //   test.shellSort(optimistic);//optymistyczny
        // //    test.bubbleSort(pesymistic);//pesymistyczny
////    test.insertionSort(pesymistic);//pesymistyczny
        // test.quickSort(pesymistic,0,pesymistic.length-1);//pesymistyczny
          test.heapSort(pesymistic);//pesymistyczny
        //   test.shellSort(pesymistic);//pesymistyczny
        //    test.bubbleSort(realistic);//realistyczny
//       test.insertionSort(realistic);//realistyczny
         //test.quickSort(realistic,0,realistic.length-1);//realistyczny
        //   test.heapSort(realistic);//realistyczny
        //   test.shellSort(realistic);//realistyczny
        long tEnd = System.currentTimeMillis();
        long tDelta = tEnd - tStart;
        double elapsedSeconds = tDelta / 1000.0;

        System.out.println("Czas sortowania: "+elapsedSeconds);
    }
}
