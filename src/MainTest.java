import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.beans.Expression;
import java.util.ArrayList;
import java.util.List;

import static java.time.Duration.ofMillis;
import static java.time.Duration.ofSeconds;
import static org.junit.jupiter.api.Assertions.*;

class MainTest {
    Main main;

    @BeforeEach
    public void setUp() throws Exception{
        main=new Main();
    }

    @Test
    public void checkIfSolutionGiveMinValue(){
        List<Integer> list=new ArrayList<>();
        list.add(4);
        list.add(6);
        Assertions.assertEquals(4,main.solution(list));
    }

    @Test
    public void checkIfSolutionDoesntGiveWrong(){
        List<Integer> list=new ArrayList<>();
        list.add(4);
        list.add(6);
        Assertions.assertNotEquals(6,main.solution(list));
    }
    @Test
    public void throwsExceptionWhenArgumentIsWrong(){
        List<Integer> list=new ArrayList<>();
        list.add(4);
        list.add(6);
        list.add(10000000);
        assertThrows(RuntimeException.class, () -> {
            main.solution(list);
        });
    }
    @Test
    public void checkIfSubstringWorksCorretly(){

        Assertions.assertEquals(0,main.substring("a","a"));
        Assertions.assertEquals(2,main.substring("a","aa"));
        Assertions.assertEquals(3,main.substring("ab","babab"));
    }

    @Test
    public void checkIfSubstringthrowsException(){
        assertThrows(RuntimeException.class, () -> {
            main.substring(null,"a");
        });
    }

    @Test
    public void checkIfSolution2WorksCorretly(){
        float[] tablicaFloatow={2,7,5,10};
        int []expectedResult={0,2};
        Assertions.assertArrayEquals(expectedResult,main.solution2(tablicaFloatow,7));
    }

    @Test
    public void checkIfSolution2throwsException(){
        assertThrows(RuntimeException.class, () -> {
            float[] tablicaFloatow={2,7,5,10};
            main.solution2(tablicaFloatow,222);
        });
    }

    @Test
    public void checkIfzadanie5Time(){
        assertTimeout(ofMillis(100), () -> {
            // This method runs in 10 seconds
            main.testZad5();
        });
    }

}